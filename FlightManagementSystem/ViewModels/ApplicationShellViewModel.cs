﻿using FlightManagementSystem.DatabaseTools;
using FlightManagementSystem.Model;
using FlightManagementSystem.ViewModels.BaseViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FlightManagementSystem.ViewModels
{
    public class ApplicationShellViewModel : ViewModelBase
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; OnPropertyChanged("FirstName"); }
        }

        public string UserFriendlyAccessLevel
        {
            get 
            {
                if (PermissionLevel == 0)
                    return "Registered Customer";
                else if (PermissionLevel == 1)
                    return "Staff Member";
                else if (PermissionLevel == 2)
                    return "Management";
                else if (PermissionLevel == 3)
                    return "Systems Administrator";
                else
                    return "Unknown";
            }
        }

        public int permissionLevel; 
        public int PermissionLevel
        {
            get { return permissionLevel; }
            set { permissionLevel = value; OnPropertyChanged("PermissionLevel"); OnPropertyChanged("UserFriendlyAccessLevel"); }
        }

        private bool notLoggedIn;
        public bool NotLoggedIn
        {
            get { return notLoggedIn; }
            set { notLoggedIn = value; OnPropertyChanged("NotLoggedIn"); }
        }

        private ViewModelBase currentViewModel;
        public ViewModelBase CurrentViewModel 
        {
            get { return currentViewModel; }
            set { currentViewModel = value; OnPropertyChanged("CurrentViewModel"); OnPropertyChanged("IsMenuVisible"); } 
        }

        private ObservableCollection<MenuButton> menuOptions;
        public ObservableCollection<MenuButton> MenuOptions 
        {
            get { return menuOptions; } 
        }

        private ObservableCollection<Notification> userNotifications;
        public ObservableCollection<Notification> UserNotifications 
        {
            get { return userNotifications; } 
        }

        public bool IsMenuVisible
        {
            get 
            {
                if (CurrentViewModel != null)
                    return false;
                else
                    return true;
            }
        }

        public ApplicationShellViewModel()
        {
            menuOptions = new ObservableCollection<MenuButton>();
            userNotifications = new ObservableCollection<Notification>();

            PermissionLevel = 0;
            NotLoggedIn = true;

            MenuOptions.Add(new MenuButton
            {
                ButtonText = "Book a Flight",
                ButtonCommand = BookFlightCommand,
                PermissionLevel = 0
            });

            MenuOptions.Add(new MenuButton
            {
                ButtonText = "Retrieve a Flight",
                ButtonCommand = RetrieveFlightCommand,
                PermissionLevel = 0
            });
        }

        private ICommand signInCommand;
        public ICommand SignInCommand
        {
            get { return signInCommand = signInCommand ?? new RelayCommand(ExecuteSignInCommand); }
        }

        private async void ExecuteSignInCommand(object obj)
        {
            MySqlConnection connection = new MySqlConnection(DatabaseStrings.ConnectionString);
            MySqlCommand command = new MySqlCommand(DatabaseStrings.SignInQuery, connection);

            command.Parameters.AddWithValue("@EmailAddress", EmailAddress);
            command.Parameters.AddWithValue("@Password", Password);

            await connection.OpenAsync();
            
            IDataReader reader = await command.ExecuteReaderAsync();

            if (reader.Read())
            {
                FirstName = reader.GetString(2);
                PermissionLevel = reader.GetInt32(3);

                reader.Close();
                connection.Close();

                NotLoggedIn = false;
            }
            else
            {
                reader.Close();
                connection.Close();
                MessageBox.Show("Login Failed");
            }

            PopulateMenu();
        }

        private void PopulateMenu()
        {
            MenuOptions.Clear();

            MenuOptions.Add(new MenuButton
            {
                ButtonText = "Book a Flight",
                ButtonCommand = BookFlightCommand,
                PermissionLevel = 0
            });

            MenuOptions.Add(new MenuButton
            {
                ButtonText = "Retrieve a Flight",
                ButtonCommand = RetrieveFlightCommand,
                PermissionLevel = 0
            });

            if (PermissionLevel >= 0)
            {
                UserNotifications.Add(new Notification
                {
                    NotificationText = "Successfully Logged In",
                    DismissNotification = DismissNotificationCommand,
                    Urgent = false
                });

                UserNotifications.Add(new Notification
                {
                    NotificationText = "Ugent Item!",
                    DismissNotification = DismissNotificationCommand,
                    Urgent = true
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "View My Active Bookings",
                    ButtonCommand = null,
                    PermissionLevel = 0
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "View My Notifications",
                    ButtonCommand = null,
                    PermissionLevel = 0
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Alter My Account Details",
                    ButtonCommand = null,
                    PermissionLevel = 0
                });
            }

            if (PermissionLevel >= 1)
            {
                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Retrieve Customer Details",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });
            }

            if (PermissionLevel >= 2)
            {
                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Report Generation",
                    ButtonCommand = null,
                    PermissionLevel = 2
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Change Flight Data",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Add a New Flight",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Add a New Aircraft",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Add a New Airport",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Add a New Route",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Alter Available Inflight Service Options",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });
            }
            
            if (PermissionLevel >= 3)
            {
                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Enable Account / Disable Accounts",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });

                MenuOptions.Add(new MenuButton
                {
                    ButtonText = "Reset User Password",
                    ButtonCommand = null,
                    PermissionLevel = 1
                });
            }
        }

        private ICommand signOutCommand;
        public ICommand SignOutCommand
        {
            get { return signOutCommand = signOutCommand ?? new RelayCommand(ExecuteSignOutCommand); }
        }

        private void ExecuteSignOutCommand(object obj)
        {
            CurrentViewModel = null;
            menuOptions.Clear();
            PermissionLevel = 0;
            EmailAddress = null;
            OnPropertyChanged("CustomerID");
            Password = null;
            OnPropertyChanged("Password");
            FirstName = null;
            NotLoggedIn = true;

            MenuOptions.Add(new MenuButton
            {
                ButtonText = "Book a Flight",
                ButtonCommand = BookFlightCommand,
                PermissionLevel = 0
            });

            MenuOptions.Add(new MenuButton
            {
                ButtonText = "Retrieve a Flight",
                ButtonCommand = RetrieveFlightCommand,
                PermissionLevel = 0
            });
        }

        private ICommand bookFlightCommand;
        public ICommand BookFlightCommand
        {
            get { return bookFlightCommand = bookFlightCommand ?? new RelayCommand(ExecuteBookFlightCommand); }
        }

        private void ExecuteBookFlightCommand(object obj)
        {
            MessageBox.Show("Flight Booked!");
        }

        private ICommand retrieveFlightCommand;
        public ICommand RetrieveFlightCommand
        {
            get { return retrieveFlightCommand = retrieveFlightCommand ?? new RelayCommand(ExecuteRetrieveFlightCommand); }
        }

        private void ExecuteRetrieveFlightCommand(object obj)
        {
            CurrentViewModel = new RetrieveBookingViewModel();
        }

        private ICommand returnHomeCommand;
        public ICommand ReturnHomeCommand
        {
            get { return returnHomeCommand = returnHomeCommand ?? new RelayCommand(ExecuteReturnHomeCommand); }
        }

        private void ExecuteReturnHomeCommand(object obj)
        {
            CurrentViewModel = null;
        }

        private ICommand signUpCommand;
        public ICommand SignUpCommand
        {
            get { return signUpCommand = signUpCommand ?? new RelayCommand(ExecuteSignUpCommand); }
        }

        private void ExecuteSignUpCommand(object obj)
        {
            CurrentViewModel = new SignUpViewModel();
        }

        private ICommand dismissNotificationCommand;
        public ICommand DismissNotificationCommand
        {
            get { return dismissNotificationCommand = dismissNotificationCommand ?? new RelayCommand(ExecuteDismissNotificationCommand); }
        }

        private void ExecuteDismissNotificationCommand(object obj)
        {
            CurrentViewModel = new SignUpViewModel();
        }
    }
}
