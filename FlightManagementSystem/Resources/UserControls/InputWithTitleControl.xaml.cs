﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlightManagementSystem.Resources.UserControls
{
    /// <summary>
    /// Interaction logic for InputWithTitleControl.xaml
    /// </summary>
    public partial class InputWithTitleControl : UserControl
    {
        public string UCTitle
        {
            get { return (string)GetValue(UCTitleProperty); }
            set { SetValue(UCTitleProperty, value); }
        }

        public string UCContent
        {
            get { return (string)GetValue(UCContentProperty); }
            set { SetValue(UCContentProperty, value); }
        }

        public string UCContentHint
        {
            get { return (string)GetValue(UCContentHintProperty); }
            set { SetValue(UCContentHintProperty, value); }
        }

        public InputWithTitleControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty UCTitleProperty =
            DependencyProperty.Register("UCTitle", typeof(string), 
            typeof(InputWithTitleControl), new PropertyMetadata(""));

        public static readonly DependencyProperty UCContentProperty =
            DependencyProperty.Register("UCContent", typeof(string),
            typeof(InputWithTitleControl), new PropertyMetadata(""));

        public static readonly DependencyProperty UCContentHintProperty =
            DependencyProperty.Register("UCContentHint", typeof(string),
            typeof(InputWithTitleControl), new PropertyMetadata(""));
    }
}
