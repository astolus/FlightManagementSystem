﻿using FlightManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlightManagementSystem.Resources.UserControls
{
    /// <summary>
    /// Interaction logic for ApplicationTopBar.xaml
    /// </summary>
    public partial class ApplicationTopBar : UserControl
    {
        public ApplicationTopBar()
        {
            InitializeComponent();
        }

        private void OpenNotificationsPopup(object sender, RoutedEventArgs e)
        {
            NotificationsPopup.IsOpen = true;
        }

        private void CloseNotificationsPopup(object sender, RoutedEventArgs e)
        {
            NotificationsPopup.IsOpen = false;
        }
    }
}
